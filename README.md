# docker for webcit

校務支援システム開発・運用のための docker files

## Use for development

docker の ENTRYPOINT で bundle install を実行するために、[entrykit](https://github.com/progrium/entrykit) を使用しています。
開発版では docker image を Gemfile の変更ごとに image を再作成しないように bundle gems はホスト側に書き出しています。
開発環境を一から作成するには、以下のコマンドを実行します。

```bash
git clone https://bitbucker.org/hkob/docker-webcit.git
cd docker-webcit
script/init && script/bootstrap
# access to http://localhost:3000
```

作成されるコンテナは以下の 6 つです。
- webcitc-d
  - 教職員用サーバのメインコンテナです。
- webpack-webcitc-d
  - 教職員用サーバの webpacker-dev-server です。
- webcit-d
  - 学生用サーバのメインコンテナです。
- webpack-webcit-d
  - 学生用サーバの webpacker-dev-server です。
- postgres
  - データベースを保持する PostgreSQL コンテナです。
- platex-dev
  - 帳票を作成する LaTeX コンテナです。

また、このコマンドにより、docer-webcit と同階層に data-webcit ディレクトリが作成されます。
ここには、以下のフォルダが用意されます。
- dev-bundle
  - bundle の置き場です。/usr/local/bundle が書き出されます。4つのコンテナから利用されます。
- dev-db
  - postgresql のデータベース置き場です。postgres コンテナの /var/lib/postgresql/data が書き出されます。
- webcitc_app:
  - 教職員用サーバの Rails アプリ置き場です。webcitc-d, webpack-webcit-d の /app が書き出されます。アプリ開発はここで行います。
- webcit_app:
  - 学生用サーバの Rails アプリ置き場です。webcit-d, webpack-webcit-d の /app が書き出されます。

## License

MIT
